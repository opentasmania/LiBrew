#!/usr/bin/python3 -I
#
# Copyright (c) 2012-2015 Stephen P. Smith
# Copyright (C) Peter Lawler <relwalretep@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
# IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from logging import basicConfig, getLogger
from os import path
from subprocess import Popen, PIPE
from traceback import format_exc

from globals import DEBUG_LEVEL

basicConfig()
librew_tw1_log = getLogger('librew-temp1wire')
librew_tw1_log.setLevel(DEBUG_LEVEL)
librew_tw1_log.propagate = True
librew_tw1_log.debug("logger constructed")


class Temp1Wire:
    numSensor = 0

    def __init__(self, tempSensorId):
        self.tempSensorId = tempSensorId
        self.sensorNum = Temp1Wire.numSensor
        Temp1Wire.numSensor += 1
        # Raspbian build in January 2015 (kernel 3.18.8 and higher)
        # has changed the device tree.
        oldOneWireDir = "/sys/bus/w1/devices/w1_bus_master1/"
        newOneWireDir = "/sys/bus/w1/devices/"
        if path.exists(oldOneWireDir):
            self.oneWireDir = oldOneWireDir
            librew_tw1_log.debug("Old 1Wire directory %s " % (oldOneWireDir))
        else:
            self.oneWireDir = newOneWireDir
            librew_tw1_log.debug("New 1Wire directory %s " % (newOneWireDir))
        librew_tw1_log.info("Constructing 1W sensor %s" % (tempSensorId))

    def readTempC(self):
        temp_C = -99  # default to assuming a bad temp reading

        if path.exists(self.oneWireDir + self.tempSensorId + "/w1_slave"):
            tb = ""
            try:
                pipe = Popen(["cat", self.oneWireDir + self.tempSensorId +
                              "/w1_slave"], stdout=PIPE)
                result = pipe.communicate()[0].decode('utf-8')
            except:
                tb = format_exc()
            if (result.split('\n')[0].split(' ')[11] == "YES"):
                temp_C = float(result.split("=")[-1]) / 1000  # temp in Celcius
            else:
                librew_tw1_log.error("Error on oneWire read for %s" % (self.tempSensorId))
                librew_tw1_log.error("traceback %s" % (tb))
        else:
            librew_tw1_log.warning("Sensor missing %s" % (self.tempSensorId))

        return temp_C
