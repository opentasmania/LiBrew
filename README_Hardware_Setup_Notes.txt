1) P9-1 (GND) to GND on breadboard
2) P9-3 (3v3) to breadboard
3) Install 1Wire overlay by either
. Connect 1-W data line (temp_sensor_id) to P9.12 https://github.com/BeagleBoard/bb.org-overlays/src/arm/BB-W1-P9.12-00A0.dts
** OR **
. Connect 1-W data line (temp_sensor_id) to P9.27 https://gitlab.com/petelawler/PL-BB-overlays/raw/master/PL-W1-P9.27-00A0.dts
4) Connect relay (heat_pin) to P8.7. Additional heat_pins recommended on P8.8, P8.9 & P8.10)

### Suggestions
5) Connect SMS monitoring to UART4. P9.11 (UART4_RXD GPIO0[30]) -> FONA PIN TX, P9.13 (UART4_TXD GPIO0[31]) -> FONA PIN RX
6) Connect UPS to USB, using nut for monitoring and SMS for notifications
7) Add systemd scripts for SMS status updates
8) RTC
