#!/usr/bin/python3 -I
import json
import xmltodict
 
with open("config.xml", 'r') as f:
    xmlString = f.read()
 
print("XML input (config.xml):")
print(xmlString)
     
jsonString = json.dumps(xmltodict.parse(xmlString), indent=4)
 
print("\nJSON output(librew_config.json):")
print(jsonString)
 
with open("librew_config.json", 'w') as f:
    f.write(jsonString)
